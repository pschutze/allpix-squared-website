#!/bin/bash

# SPDX-FileCopyrightText: 2022 CERN and the Allpix Squared authors
# SPDX-License-Identifier: MIT

# exit on error
set -e

# help text
help() {
    echo "Usage: get_artifacts.sh [mode] [args...]"
    echo " for ref mode: get_artifacts.sh ref [doxy] [ref_name [repo_name]]"
    echo " for job mode: get_artifacts.sh job [doxy] [job_nr [repo_name]]"
    echo " add \"doxy\" to download API reference instead of manual"
    echo "Defaults:"
    echo " mode: ref"
    echo " ref_name: master"
    echo " repo_name: allpix_squared/allpix_squared"
    echo " job_nr: no default value"
    exit
}

# check script mode, default: download by ref
mode="$1"
if [ -z "$mode" ]; then mode="ref"; fi

# store arguments for later
arg="$2"
repo="$3"
job_name="docs:usermanual-hugo"

# check whether to download doxygen API reference
doxy=false
if [ "$2" == "doxy" ]; then
    doxy=true
    arg="$3"
    repo="$4"
    job_name="docs:doxygen"
    echo "Downloading doxygen API reference"
fi

# use upstream repo by default
if [ -z "$repo" ]; then repo="allpix-squared/allpix-squared"; fi

# set URL depending on mode
if [ "$mode" == "ref" ]; then
    ref_name="$arg"
    if [ -z "$ref_name" ]; then ref_name="master"; fi
    url="https://gitlab.cern.ch/$repo/-/jobs/artifacts/$ref_name/download?job=$job_name"
    echo "Using repo \"$repo\" with ref \"$ref_name\"."
elif [ "$mode" == "job" ]; then
    job_nr="$arg"
    if [ -z "$job_nr" ]; then help; fi
    url="https://gitlab.cern.ch/$repo/-/jobs/$job_nr/artifacts/download"
    echo "Using repo \"$repo\" with job \"$job_nr\"."
else
    help
fi

# cd to script location
pushd $(dirname "$0") > /dev/null

# create temporary path
tmpdir="$XDG_RUNTIME_DIR/allpix-squared-website"
rm -rf $tmpdir
mkdir -p $tmpdir
pushd $tmpdir > /dev/null

# downloading artifacts
echo "Downloading artifacts..."
echo "URL: $url"
wget -nv $url -O artifacts.zip

# extracting artifacts
echo "Extracting artifacts..."
unzip -q artifacts.zip

# copying files
echo "Copying files..."
popd > /dev/null
if [ $doxy == true ]; then
    rm -rf content/reference/*/
    mv $tmpdir/public/reference_markdown/* content/reference/
else
    rm -rf content/docs/*/
    mv $tmpdir/public/usermanual_hugo/* content/docs/
fi

# cleanup
popd > /dev/null
rm -rf $tmpdir
echo "Done, run \"hugo server\" to view the website."
