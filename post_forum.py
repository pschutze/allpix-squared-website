#!/usr/bin/env python3

import logging
import pathlib
import re
import sys

import git
import requests
import yaml


def main(api_key: str, commit_before_sha: str, commit_sha: str) -> int:
    return_code = 0

    logging.info(f'Iterating through changes in diff from {commit_before_sha} to {commit_sha}')

    # get git repo
    repo_path = pathlib.Path(__file__).parent
    repo = git.Repo(repo_path)

    # iterate through added posts
    for diff in repo.commit(commit_before_sha).diff(commit_sha, paths='content/post/*.md').iter_change_type('A'):
        file_path_rel = pathlib.Path(diff.b_path)
        file_path_abs = repo_path.joinpath(file_path_rel)
        logging.info(f'Processing {file_path_rel}')
        # get title and excerpt until more tag
        excerpt = str()
        title = str()
        with open(file_path_abs, mode='rt', encoding='utf-8') as file:
            file_content = file.read()
            # match yaml part to get tile
            yaml_match = re.match(r'^---\n(.+?)\n---\n\n?', file_content, flags=re.DOTALL)
            yaml_data = yaml.safe_load(yaml_match.group(1))
            title = yaml_data['title']
            logging.debug(f'Title: "{title}"')
            file_content_after_yaml = file_content[yaml_match.end(0):]
            # get excerpt, first check if we have a more tag
            more_tag_match = re.match(r'^(.+)(<!--more-->.*)', file_content_after_yaml, flags=re.DOTALL)
            if more_tag_match:
                logging.debug('Found <!--more--> tag')
                excerpt = more_tag_match.group(1)
            # if not, set excerpt to entire text but print warning since there might be hugo shortcodes or images in there
            else:
                logging.warning('No <!--more--> tag found, please add tag to post next time to ensure only plain markdown '
                                'without any images or hugo shortcodes are posted to discourse! Trying to upload anyway.')
                return_code += 1
                excerpt = file_content_after_yaml
            logging.debug(f'Excerpt: "{excerpt}"')
        # get url of post for embedding
        embed_url = f'https://allpix-squared.docs.cern.ch/post/{file_path_rel.stem}/'
        logging.debug(f'Embed URL: "{embed_url}"')
        # send POST request
        headers = {'Content-Type': 'application/json', 'Api-Username': 'system', 'Api-Key': api_key}
        data = f'{{"title":"{title}","category":8,"raw":"{excerpt}","embed_url":"{embed_url}"}}'
        resp = requests.post('https://allpix-squared-forum.web.cern.ch/posts.json', headers=headers, data=data, timeout=10)
        logging.info(f'Request sent, status code of response: {resp.status_code}')
        # check response code, if not increment return code for script failure
        if resp.ok:
            logging.info('Status code ok')
        else:
            logging.error('Status code indicates failure!')
            return_code += 1

    return return_code

if __name__ == '__main__':
    logging.basicConfig(format='(%(levelname)s) %(message)s', level=logging.INFO)
    if len(sys.argv) != 4:
        print('Requires 3 arguments: api_key, commit_before_sha and commit_sha')
        sys.exit(255)
    else:
        sys.exit(main(sys.argv[1], sys.argv[2], sys.argv[3]))
