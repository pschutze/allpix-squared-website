---
# SPDX-FileCopyrightText: 2022 CERN and the Allpix Squared authors
# SPDX-License-Identifier: CC-BY-4.0
title: Community
menu:
  main:
    weight: 70
---

{{< blocks/lead height="auto" color="secondary" >}}
<h2 style="padding-bottom: 1em;">
    Community
</h2>
<span class="apsq-lead-text">
    Allpix Squared is a collaborative, open source software project. This page contains directions where to find help and support, and how to get actively involved in the development.
</span>
{{< /blocks/lead >}}

<div class="container l-container--padded">
<div class="row">
{{% tool title="User Manual" icon="fas fa-book" url="/docs/" desc="Comprehensive user and physics manual for Allpix Squared" %}}
{{% tool title="Mattermost" icon="fas fa-comments" url="https://mattermost.web.cern.ch/allpix2" desc="Discussion channel for developers and users" %}}
{{% tool title="User Forum" icon="fa-solid fa-network-wired" url="https://allpix-squared-forum.web.cern.ch/" desc="Forum for questions and discussions" %}}
</div>
<div class="row">
{{% tool title="Workshops" icon="fas fa-users" url="https://indico.cern.ch/category/1438/search?q=allpix&sort=mostrecent" desc="Regular user workshops to present use cases and to exchange with others" %}}
{{% tool title="Mailing lists" icon="fas fa-envelope" url="https://e-groups.cern.ch/e-groups/EgroupsSearch.do?searchValue=allpix-squared" desc="Mailing list for Allpix Squared (via CERN e-groups, very low traffic)" %}}
{{% tool title="GitLab" icon="fab fa-gitlab" url="https://gitlab.cern.ch/allpix-squared/allpix-squared" desc="Main development repository (CERN GitLab, requires CERN account for write access)" %}}
</div>
<div class="row">
{{% tool title="GitHub" icon="fab fa-github" url="https://github.com/allpix-squared/allpix-squared" desc="Cloned repository at GitHub to enable contributions from people without CERN account" %}}
{{% tool title="Issue Tracker" icon="fas fa-bug" url="https://gitlab.cern.ch/allpix-squared/allpix-squared/issues/" desc="List of known issues and bugs" %}}
{{% tool title="Developer manual" icon="fas fa-laptop-code" url="/docs/10_development/" desc="Sections of the user manual related to module development and CI" %}}
</div>
</div>