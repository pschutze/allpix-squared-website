---
# SPDX-FileCopyrightText: 2022 CERN and the Allpix Squared authors
# SPDX-License-Identifier: CC-BY-4.0
title: "3rd Allpix Squared User Workshop"
date: 2022-02-25T12:08:10+01:00
draft: false
---

We are happy to announce that we will host a user workshop again this year to hear about your simulations and present the latest developments. Due to the ongoing pandemic situation and uncertain travel conditions, the workshop will be held **online from the 9th to the 11th of May** in the CET afternoons.

The aim of this workshop is to bring the users and developers together to discuss interesting applications and new features. There will be an introductory talk, case studies, expert talks and presentations about the development of the framework.

We kindly ask you to **register** through the [Indico page of the event](https://indico.cern.ch/e/apsqws3), where you can also **submit an abstract** to present your use case of or developments on Allpix Squared. We encourage contributions from all the fields that benefit from this simulation framework. The abstract submission deadline is the 18th of April.

The poster of the event can be downloaded [here](/pdf/apsq_workshop_3_poster.pdf).

{{< figure src="/img/apsq_workshop_3_poster.png">}}
