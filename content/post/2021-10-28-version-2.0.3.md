---
# SPDX-FileCopyrightText: 2021 CERN and the Allpix Squared authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Patch Release 2.0.3"
date: 2021-10-28T16:55:32+02:00
draft: false
---

We hereby announce the next patch release for the 2.0 series of **Allpix Squared, version 2.0.3**.
This release contains 36 commits over the last patch version 2.0.2 and updates some missing details in the user manual, comes with an updated CI reference build and adds some additional checks for corner cases.
The release is available as Docker image, CVMFS installation, binary tarball and source code from the [repository](https://gitlab.cern.ch/allpix-squared/allpix-squared/).

The following issues have been resolved and improvements have been made:
<!--more-->

* **Documentation**
   * Updated build flags required for Geant4 in the user manual
   * Manual can now be built again with more recent versions of Pandoc
* **Executable**: The version printing now also reports versions from dependencies (ROOT, Geant4, Boost.Random) and reports from where this executable stems (CI/CD build, local build, ...)
* **Misc:**
   * Many small improvements stemming from Coverity and linting suggestions such as potential divisions by zero in log messages
   * Updated 3rdparty library MagicEnum to version 0.7.3
   * New detector model for Timepix4 added
   * The ModuleManager now catches *undefined symbol* errors when loading dynamic libraries that have been compiled with a different version of Allpix Squared
* **Module WeightingPotentialReader:** A first test of the pad model has been added to the CI
* **Module GeometryBuilderGeant4:** This module now checks for the compiled-in multithreading capabilities of the Geant4 libraries used, and will only call `allow_multithreading()` if Geant4 supports it. If not, it forces Allpix Squared to run in single-threaded mode
* **Continuous Integration:**
   * Updated LCG to version LCG_101 and Geant4 10.7.2, coming along with GCC11 and Clang11
   * Parallel support for MacOS 10.15 and MacOS 11
   * The output from static code analysis and linting is now filtered to find the culprits quicker
   * The Doxygen documentation generation will now abort if it finds documentation errors or missing doc strings
   * Moving to building Docker images with Kaniko instead of dedicated runners, suggested by CERN IT
