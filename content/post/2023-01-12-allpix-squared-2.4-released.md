---
# SPDX-FileCopyrightText: 2023 CERN and the Allpix Squared authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Allpix Squared 2.4 Released"
date: 2023-01-12T11:36:51+01:00
draft: false
---

We are happy to announce a new stable feature release **Allpix Squared, version 2.4** with a total of **259 commits** added
to the repository by **five contributors** since version 2.3.2. The release tarball can be downloaded from the repository:

https://gitlab.cern.ch/allpix-squared/allpix-squared/tags/v2.4.0

The new version is also available as docker image in the [project's docker registry](https://gitlab.cern.ch/allpix-squared/allpix-squared/container_registry), as read-to-use [version on CVMFS](https://allpix-squared.docs.cern.ch/docs/11_devtools/04_deployment/#software-deployment-to-cvmfs) and as binary release [from the website](https://project-allpix-squared.web.cern.ch/releases/).

Most notably, Allpix Squared was extended by two modules: *DepositionGenerator* and *DepositionLaser*. In the following, an
overview over these modules as well as other important changes and new features is provided:
<!--more-->

### DepositionGenerator module

This module allows to read primary particles produced by Monte Carlo event generators such as Pythia.
[MR!922](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/922)

The module reads the primary particles generated for each event in the file, creates an Allpix Squared event and simulates
the charge deposits using Geant4.
It inherits functionality from the *DepositionGeant4* module and several of its parameters have their origin there.

Supported data formats are [HepMC3](https://doi.org/10.1016/j.cpc.2020.107310) and
[GENIE](https://doi.org/10.1016/j.nima.2009.12.009). Extending the module to other data formats is easily possible if
required.
The different file formats can be selected via the `model` parameter, the path to the data file has to be provided via the
`file_name` configuration parameter.

Example for usage:

```ini
[DepositionGenerator]
physics_list = FTFP_BERT_LIV
max_step_length = 10.0um
model = "GENIE"
file_name = "genie_input_data.root"
```

### DepositionLaser module

This module is intended for simulations of laser-TCT experiments. It generates charge deposited by absorption of a laser
pulse in silicon bulk. [MR!924](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/924)

This module implements tracking algorithms and simulations of corresponding physical phenomena by its own. It assumes that
the laser pulse is a bunch of point-like photons, each traveling in a straight line. Multiple photons are produced in *one*
event, thus a *single* event models a *single* laser pulse.

Tracking features:
* optical properties of silicon for different wavelength are available
* photons are absorbed in detector bulk on physically correct depth
* each photon is assumed to create exactly one e-h pair
* photons refract on silicon-air interface
* tracks are terminated when a photon leaves first encountered sensitive volume
* tracks are terminated if a passive object is hit (the only supported passive object type is box)

Initial direction and starting timestamp for every photon in the bunch are generated to mimic spatial and temporal
distributions of delivered intensity of a real laser pulse.

Two options for beam geometry are currently available: `cylindrical` and `converging`. For both options, transversal beam
profiles will have a gaussian shape. For a `cylindrical` beam, all tracks are parallel to the set beam direction. For a
`converging` beam, track directions would have isotropic distribution (but with a limit on a max angle between the track and
the set beam direction).

Example for usage:

```ini
[DepositionLaser]
# Standard wavelength for IR TCT lasers
wavelength = 1064nm

number_of_photons = 50000
pulse_duration = 1ns

# Geometry parameters of the beam
source_position = 0 0 -5mm
beam_direction = 0 0 1
beam_geometry = "converging"
beam_waist = 10um
focal_distance = 5mm
beam_convergence_angle = 20deg
```

### Other Notable Features and Improvements

* **Core / CI**:
    * Update CI to LCG102 [MR!921](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/921)
    * GDML processing code fixes [MR!934](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/934)
    * Backport of Liang-Barsky Method from master branch [MR!923](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/923)
    * Add Constant Effective Lifetime Trapping & Detrapping [MR!920](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/920)
    * Search path of config files for detector models by default [MR!912](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/912)
    * Do not build VisualizationGeant4 for docker images [MR!914](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/914)
    * Added extra whitespaces in stream output of MCTrack [MR!933](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/933)
* **ElectricFieldReader**: better parameter explanations in README [MR!926](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/926)
* **DopingProfileReader**: Fixed the doping profile offset and added output plots [MR!908](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/908)
* **TransientPropagation**: add graphs for induced charge vs depth, time and in-pixel position [MR!909](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/909)
