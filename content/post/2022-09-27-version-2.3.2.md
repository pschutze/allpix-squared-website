---
title: "Patch Release 2.3.2"
date: 2022-09-27T12:52:33+02:00
draft: false
---

We would like to announce the second patch release for the 2.3 series of **Allpix Squared, version 2.3.2**.
This release contains 48 commits over the feature release version 2.3.1.
The release is available as [Docker image](https://gitlab.cern.ch/allpix-squared/allpix-squared/container_registry/1137), CVMFS installation, [binary tarball](https://cern.ch/allpix-squared/releases) and source code from the [repository](https://gitlab.cern.ch/allpix-squared/allpix-squared/).

The following issues have been resolved and improvements have been made:
<!--more-->


* **Core**
   * A bug in the module instantiation logic has been discovered and fixed, where some specific input configuration files and module setups could lead to invalid memory access and a subsequent crash of the software. The instantiation priority is now calculated correctly [MR!865](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/865)
   * The `DetectorField` class now correctly returns a zero electric field in the sensor excess region defined in the detector model. Before, the electric field was wrongly replicated into the excess. While this has no impact on the simulation since charge carriers outside the pixel matrix are ignored, it still is confusing in electric field maps and plots [MR!851](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/851)
   * Minor Coverity finds have been fixed such as using a reference instead of an object copy in loops with read-only access.

* **Module TransientPropagation:** This module now allows using a linear electric field. The corresponding module error has been replaced by a `WARNING`-level log message that this configuration is prone to produce unphysical results when used with segmented electrodes [MR!857](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/857)
* **Modules ProjectionPropagation, GenericPropagation, TransientPropagation, CSADigitizer:** Additional decimal digits have been added to the Boltzmann constant used in these modules. [MR!859](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/859)

* **Module DetectorHistogrammer:**
   * In the presence of a sensor excess, the efficiency was wrongly calculated, including the passive area around the matrix. This was corrected. Furthermore, the `track_resolution` parameter received additional documentation [MR!861](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/861)
   * Some histogram ranges have been corrected to use the right configuration parameter
   * The ToA calculation has been made independent of the pulse polarity [MR!856](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/856)
   * In some configurations, the global timestamp produced for `PixelHit` objects could be a mixture of nanoseconds and ToA units. Now, the global timestamp is *always* provided in nanoseconds, the documentation has been amended accordingly [MR!863](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/863)
* **Manual:** The manual now correctly states `kT` (kil Tesla) as the default framework unit for magnetic fields instead of `T` (Tesla), which was wrongly given before [MR!836](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/836). Please always remember to specify units in configuration files!
* **Miscellaneous:**
   * The `make_module.sh` script can now handle modules without input messages (empty prompt) and the *DummyModule* documentation has been reworked, modernized and extended for more clarity [MR!873](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/873)
   * Some leftover `strcmp` comparisons have been replaced with C++ `std::string` comparisons in the executable
   * The Docker image version of the EOS web deploy worker has been temporarily pinned until an upstream issue is resolved [MR!874](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/874)
