---
# SPDX-FileCopyrightText: 2023 CERN and the Allpix Squared authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Allpix Squared 3.0 Released"
date: 2023-05-04T16:30:00+01:00
draft: false
---

We are more than just happy, one might say even a bit proud, to announce the release of the next major version of the Allpix Squared framework: **Allpix Squared 3.0**.
The new release comes with a large number of new features of which you can read below and contains more than 2100 commits over the last feature release, version 2.4.

A direct link to the source code can be found here:

https://gitlab.cern.ch/allpix-squared/allpix-squared/tags/v3.0.0

The new version is also available as docker image in the [project's docker registry](https://gitlab.cern.ch/allpix-squared/allpix-squared/container_registry) and as read-to-use [version on CVMFS](https://allpix-squared.docs.cern.ch/docs/11_devtools/04_deployment/#software-deployment-to-cvmfs).

We would like to thank everyone who contributes to this project. To date, we count 52 contributors - thank you for your input and your will to share your work with the community! In addition, we express gratitude to all those who contributed via fruitful discussions and thorough testing of new as well as existing features.

We would also like to advertise our [**fourth User Workshop**](https://indico.cern.ch/e/apsqws4), which will be held at DESY from 22 May 2023 to 23 May 2023.
Although the in-person registration deadline has passed, there is still the chance to register for a remote participation and learn about news around the development and applications of Allpix Squared.

In the following, we will briefly present some of the more prominent new features and changes to the framework:
<!--more-->

* [Website & Documentation](#website-and-documentation)
* [Detector Geometries](#detector-geometries)
* [Impact Ionization](#impact-ionization)
* [New Simulation Objects](#new-simulation-objects)
* [Additional Features & Changes](#more)
* [Development Visualization](#gource)


## Website and Documentation

For Allpix Squared 3.0, the documentation and the entire website has been reworked. A major part of the rework has been the
transition from LaTeX as main documentation format to Markdown. Using Markdown means the documentation can be read directly
in GitLab/GitHub and in many IDEs. Using Markdown also fixes many of the display issues that existed on the old online
documentation, so we suggest you to give it a try! Besides the improved online documentation, the PDF manual also continues
to exist.

The new website is now hosted on [allpix-squared.docs.cern.ch](https://allpix-squared.docs.cern.ch). Some of the new features
are responsive design and a site-wide search function. The new website now also hosts the API reference in the same design.
But again, we recommend to just try it out.

## Detector Geometries

In Allpix Squared 3.0, the detector geometry subsystem has seen a major overhaul, and now features multiple different sensor geometries and more flexibility in combining sensor and readout ASIC.

### Detector Assemblies

From Allpix 3.0 onwards, the sensor geometry and the sensor assembly type can be specified separately in the detector model file, e.g.

```ini
type = "hybrid"
geometry = "hexagon"
```

to combine a hexagonal pixel sensor with a separate readout ASIC connected via bump bonds, or

```ini
type = "monolithic"
geometry = "pixel"
```

for a monolithic active pixel sensor with rectangular pixels.

### Hexagonal Pixel Detectors

One of the newly implemented sensor geometries are hexagonal pixel grids as used in many X-ray detectors or photon science applications. This implementation has been around for quite some time already and has been tested by multiple projects.

Allpix Squared implements hexagons using **axial coordinates**, since they

* make many calculations quite simple and fast since they derive from a projection of cubic Cartesian coordinates
* require only two coordinates and therefore fit well into the framework and the indices in Cartesian coordinates already available.
* minimize required memory footprint by storing only two and not the third (redundant) cubic coordinate. The latter can always be reconstructed from the first two as $`z = - x - y`$.

These two orientations of hexagons are referred to as **pointy** (with sides parallel to the y axis of the Cartesian coordinate system and corners at the top and bottom) and **flat** (with sides parallel to the Cartesian x axis and corners to the left and right) in Allpix Squared.
The pitches $`p_x`$ and $`p_y`$ have been chosen as the diameter of the hexagon at two adjacent corners. They align with the axial coordinate system and are oriented differently with respect to the Cartesian system for the two variants, respectively.

{{< figure src="/img/hexagon_orientations.png">}}

These hexagons can be assembled to grids in two different orientations:

{{< figure src="/img/hexagon_grids.png">}}

It should be noted that with two pitches defined, irregular (or stretched) hexagons can be used as well, simply by providing different sizes for pitch $`p_x`$ and $`p_y`$.

As always, a more detailed description can be found [in the user manual](https://allpix-squared.docs.cern.ch/docs/05_geometry_detectors/03_modelgeom/#hexagonal-pixels).

For the interested reader, we recommend [this excellent overview of hexagonal grids](https://www.redblobgames.com/grids/hexagons/), which nicely introduces the math required to handle hexagonal pxiel grids and the coordinate transformations to and from Cartesian local coordinates. Not only are the explanations very clear, there are also very nice interactive animations which help in getting a better feeling for how these grids work mathematically.


### 3D Pixel Sensors

In Allpix Squared 2.x it was only possible to define an implant region at the sensor surface, from which charge carriers could be collected. In order to make this more flexible and powerful, a new implant definition system has been introduced in Allpix Squared 3.0.

Instead of defining a single implant as part of the detector model, it is now possible to define as many implants as necessary, in a similar fashion as currently implemented for support layers:

```ini
type = "monolithic"
number_of_pixels = 3 3
pixel_size = 250um 50um
sensor_thickness = 50um

[implant]
type = frontside
shape = ellipse
size = 13um 13um 50um
offset = 62.5um 0

[implant]
type = backside
shape = rectangle
orientation = 45deg
size = 10um 40um 25um
offset = -62.5um 0
```

This system is rather flexible in defining different sensor geometries and also to support additional ohmic backside columns often found in 3D sensor designs. It should be noted, that the computing time increases with more implants as all of them need to be checked for collision with charge carriers during propagation.

With the improvement in charge carrier transportation, also the position interpolation at sensor and implant surfaces has been significantly improved.
This is especially important when running transient simulations where the weighting potential might be largest very close to the surface.
For the sensor as well as rectangular implant definitions, the Liang-Barsky line clipping algorithm is implemented to provide fast and efficient intersection calculations. For ellipsoid implants, intersections with the cylinder and its caps are considered.

More details on sensor implants [are provided in the user manual](https://allpix-squared.docs.cern.ch/docs/05_geometry_detectors/02_models/#implants).


### Radial strips

Both in the past and the future, particle physics experiments implement radial silicon strip detectors, and a new detector model caters to these needs. The implementation includes many parameters such as the possibility to define stereo angles.

This new detector model is described [in the user manual](https://allpix-squared.docs.cern.ch/docs/05_geometry_detectors/03_modelgeom/#radial-strips) and in addition, there are examples demonstrating the use of the model with [a single sensor](https://allpix-squared.docs.cern.ch/docs/09_examples/radial_strip/) as well as [a full petal of the ATLAS ITk Uprgade](https://allpix-squared.docs.cern.ch/docs/09_examples/atlas_itk_petal/).


## Detector Fields

With new possibilities of defining sensor geometries, also new ways of mapping electric fields from finite element simulations into the sensor plane have been devised. Since this has dratsically extended the possibilities of mapping pixel fields, a [new section has been added](https://allpix-squared.docs.cern.ch/docs/04_framework/05_fieldmaps/) to the user manual, describing the different options.

The following types of fields can be loaded and mapped to individual pixels with Allpix Squared 3.0:

{{< figure src="/img/maps_types.png" caption="Examples for pixel geometries in field maps. The dark spot represents the pixel center, the red extent the electric field. Pixel boundaries are indicated with a dotted line where applicable." >}}

Fields are always expected to be provided as rectangular maps, irrespective of the actual pixel shape. Maps are loaded once and assigned on a per-pixel basis. Depending on the symmetries of the pixel unit cell and the pixel grid, different geometries are supported as indicated in the figure above. The field for a quarter of the pixel plane, for half planes (see figures below) as well as for full planes (see figure above). The size of the field is not limited to a single pixel cell, however, for some quantities such as the electric field only the volume within the pixel cell is used and periodic boundary conditions are assumed and expected. Larger fields are for example useful for the weighting potential, where also potential differences to neighboring pixels are of interest.

A special case is the field presented in the right panel of the figure above. Here, the field is not centered at the pixel unit cell center, but at the corner of four adjacent rectangular pixels.

In addition, the manual now also contains [a section detailing weighting potential lookups](https://allpix-squared.docs.cern.ch/docs/04_framework/05_fieldmaps/#weighting-potential-maps--induction) and the mechanism used to calculate induced currents with the Shockley-Ramo theorem.

## Impact Ionization

Many particle detectors rely on the effect of impact ionization. Allpix Squared has been extended by several models for impact ionization, which is now available in the `GenericPropagation` and `TransientPropagation` modules. This allows among others to study the behaviour of Low Gain Avalanche Detecotors (LGAD) and alike, among others simulating the induced current over time as shown below:

{{< figure src="/img/lgad_pulse.png">}}

The implementation of impact ionization in the framework is two-fold: one part is modeling the gain factor, the other part is the microscopic simulation of charge carrier multiplication.

### Impact Ionization Models

Impact ionization models following the parametrizations of Massey, Overstraten-De Man, Okotu-Crowell and the Bologna model have been implemented. In addition, a custom impact ionization model has been added, allowing for the definition of a custom dependency of the local gain coefficient on the electric field. For the first three models, both the original parametrization and the optimized parametrizations found by the RD50 group can be selected.

The full list of implemented models along with their parameters and links to the source papers can be found [in the user manual](https://allpix-squared.docs.cern.ch/docs/06_models/05_impact_ionization/).

### Charge Carrier Multiplication

A key part in the simulation of impact ionization is the microscopic simulation of charge carrier multiplication. Allpix Squared applies a randomized approach, where the number of charge carriers generated via impact ionisation per propagation step is determined from the local gain by drawing a random number from a geometric distribution with the mean being the local gain coefficient for every individual charge carrier in one group. The latter is calculated using the user-selected impact ionization model and the average of the electric fields at the beginning and the end of the corresponding step.

This algorithm results in a mean number of secondaries generated equal to
```math
\langle n_{total}\rangle = \exp\left(\int_{x_0}^{x_n}\alpha(x)dx \right)
```
for sufficiently low step sizes. Here, $`\alpha`$ denotes the ionization coefficient calculated from the impact ionization model.

This approach leads to a total gain that does not show a trend as a function of the propagation step size, but significantly improves in precision for small propagation step sizes, as the sampling of the local gain coefficient is performed via the stepping of the particle.


## New Simulation Objects

With growing capabilities of the framework, also new ways of storing information are required.
Allpix Squared 3.0 has therefore seen an extension of its range of simulation objects that can be stored to file, but also an expansion of existing objects with additional information.

### PixelPulse

A new object was introduced, representing a post-amplification pulse from the front-end in a single pixel. It is a possibility to store time dependent signals from amplifier simulations and can, if available, be obtained from a `PixelHit`. To date, it is used as an output object of the `CSADigitizer` module.


### CarrierState: What's the Status?

As we care about the fate of our charge carriers, the `PropagatedCharge` object now stores the carrier state. It indicates the fate of the charge carrier at its end point:

* `CarrierState::UNKNOWN`: The final state of the charge carrier is unknown, it might not have been provided by the propagation algorithm
* `CarrierState::MOTION`: The charge carrier was still in motion when the propagation routine finished, for example when the configured integration time was reached
* `CarrierState::RECOMBINED`: The charge carrier has recombined with the silicon lattice at the given position
* `CarrierState::TRAPPED`: The charge carrier has been trapped by a lattice defect at the given position
* `CarrierState::HALTED`: The motion of the charge carrier has stopped, for example because it has reached an implant or the sensor surface

Besides being available when doing offline analysis, with [MR!592](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/592)
it is now also possible to generate linegraphs only for charge carriers that ended with a specific carrier state. The options
are `output_linegraphs_collected`, `output_linegraphs_recombined` and `output_linegraphs_trapped`, if supported by the
propagation algorithm.


## Additional Features & Changes {#more}

* **Physics Models**:
  * Introduce check whether material is suitable for mobility selection
  * Add custom recombination model [MR!986](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/986)
  * Add Levinshtein mobility for Gallium Nitride sensors
  * Add models for constant trapping and detrapping times
* **Materials**:
  * Add active materials: Germanium, Gallium nitride
  * Add passive materials: Boron-10, Titanium, Titanium Grade 5
  * Make use of Geant4's NistManager for the definition of elements [MR!884](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/884)
* **DepositionLaser**:
  * Add possibility to group photons to improve performance [MR!987](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/987)
  * Check for detector material [MR!927](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/927)
* **TransientPropagation**:
  * Introduce linegraphs [MR!804](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/804)
* **DepositionGeant4**:
  * Stop tracking of particles if kinetic energy is below charge creation energy [MR!879](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/879)
  * Limit maximum track length to catch some Geant4 tracking issues [MR!834](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/834)
  * Add parameter to set maximum number of charge groups to handle unexpectedly large charge deposits [MR!788](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/788)
  * Enable the possibility to store all tracks instead of filtering by charge deposition [MR!675](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/675)
* **DefaultDigitizer**:
  * Introduce parameter `gain_function` for custom front-end gain [MR!574](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/574)
  * Allow only positive thresholds and compare absolute charge [MR!855](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/855)
* **DetectorHistogrammer**:
  * Use pixel with largest charge as seed pixel [MR!545](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/545)
  * Fix counting particles traversing the sensor excess for efficiency calculation [MR!860](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/860)
  * Add plots in local coordinates [MR!607](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/607)
* **ProjectionPropagation**:
  * Introduce linegraphs [MR!804](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/804)
* **InducedTransfer**:
  * Prevent usage with information from TransientPropagation [MR!985](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/985)
* **MeshConverter**:
  * Fixed coordinate system replacement of scalar fields [MR!819](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/819)
* **Core**:
  * Introduce possibility to abort event [MR!706](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/706)
  * Prepare central detector classes for position-dependent magnetic fields [MR!779](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/779)
  * Better Interpolation at sensor edges with Liang-Barsky Method [MR!680](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/680), [MR!883](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/883)
    (already backported to v2.4)
* **Objects**:
  *  Store total deposited charge by MCParticle
* **Performance**:
  * Some improvements on locking [MR!885](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/885)
  * Performance improvements for transient analyses [MR!902](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/902)
* **Geant4**:
  * Document RunManager performance issue and allow multithreading with one worker [MR!946](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/946)
* **CI**:
  * Update LCG to LCG_103 [MR!988](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/988)
  * Add Proselint to check for common language mistakes [MR!870](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/870)
  * Add unit tests for TransientPropagation module [MR!966](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/966)
  * Fix installation of module data in CVMFS installations [MR!967](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/967)
* **Doc**:
  * Add secction on "How to generate a weighting potential in TCAD" [MR!949](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/949)
  * Improve documentation of objects in the manual
  * Move geometry descriptions to dedicated chapter [MR!899](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/899)


## Development Visualization {#gource}

Finally back - a fast-forward through the full development cycle of Allpix Squared, from its first commit up to the release of 3.0:

<div class="apsq-max-width">
{{< vimeo 824091838 >}}
</div>
