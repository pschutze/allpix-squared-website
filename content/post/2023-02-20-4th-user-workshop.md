---
title: "4th Allpix Squared User Workshop"
date: 2023-02-20T10:01:41+01:00
---

We are happy to announce that we will host a user workshop again this year to hear about your simulations and present the latest developments. The workshop will be held **on the 22nd and 23rd of May** as an in-person event at DESY. Remote participation will be possible, but in-person attendance is encouraged. Attendance of the workshop is free.

The aim of this workshop is to bring the users and developers together to discuss interesting applications and new features. There will be an introductory talk, case studies, expert talks and presentations about the development of the framework.

We kindly ask you to **register** through the [Indico page of the event](https://indico.cern.ch/e/apsqws4), where you can also **submit an abstract** to present your use case of or developments on Allpix Squared. We encourage contributions from all the fields that benefit from this simulation framework. The abstract submission deadline is the 31st of March.

We look forward to sharing an interesting workshop with you!

The poster of the event can be downloaded [here](/pdf/apsq_workshop_4_poster.pdf).

{{< figure src="/img/apsq_workshop_4_poster.png">}}
