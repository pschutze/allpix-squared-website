---
# SPDX-FileCopyrightText: 2023 CERN and the Allpix Squared authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Patch Release 3.0.1"
date: 2023-06-20T11:39:46+02:00
draft: false
---

We are happy to announce the first patch release in the 3.0 series of **Allpix Squared, version 3.0.1**.
This release contains 13 merge requests with 54 commits over the major version 3.0.0 and fixes a few issues found over the past weeks.
The release is available as Docker image, CVMFS installation and source code from the [repository](https://gitlab.cern.ch/allpix-squared/allpix-squared/).

The following issues have been resolved and improvements have been made:
<!--more-->


* **User Manual:** We continue to refine both the manual content and the manual website. With this release, we have
    * fixed a few typos and improved the layout in a few places.
    * improved Markdown to also render properly into the PDF manual.
    * included the module header information into the website representation.
    * excluded the API documentation from the website search.
    * fixed the serving of RSS feeds for our news section.
* **Core:**
    * When looking up charge carrier collisions with implants, we first check if any implants are defined before performing costly coordinate transformations.
    * When looking up electric field values, we first check if a field has been set and return otherwise before attempting coordinate transforms.
    * The `PixelDetectorModel` now has an improved method `isWithinMatrix` that does not rely on first calculating the pixel index and comparing this against the number of configured pixels. This speeds up the call significantly.
    * The material of support layers is not transformed to lower-case, because Geant4's `NistManager` has case-sensitive material lookup methods only.
* **Object PixelCharge:**
    * Fix situation where local and global reference time (and therefore any subsequently calculated time of arrival) were strictly zero.
    These timestamps are now initialized properly and set to zero only if no time could be determined from the input objects.
* **Module DopingProfileReader:**
    * Improved documentation including an example for the `regions` keyword
* **Modules GenericPropagation / TransientPropagation:**
    * New debugging plots for the gain of individual charge carrier groups as functions of `x`, `y` and `z` have been added.
* **Module CorryvreckanWriter:**
    * The module now correctly transforms `MCParticle` positions into the Corryvreckan reference coordinate system before writing them to file.
* **Module GeometryBuilderG4:**
    * An exception handler is now registered before the construction of any detector element, such that Geant4 exceptions can be caught and properly handled.
* **Continuous Integration:**
    * Altered seed for a few tests involving Geant4 to not trigger a computing-intense event in the tests
    * Switched to CentOS9 Stream for Coverity tests for compatibility reasons
