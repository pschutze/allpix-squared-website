---
# SPDX-FileCopyrightText: 2017 CERN and the Allpix Squared authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Website Live"
date: 2017-07-20T00:48:21+02:00
draft: false
---

The website of Allpix Squared is live, for now only containing the code reference. General information as well as the user manual will be added shortly.
