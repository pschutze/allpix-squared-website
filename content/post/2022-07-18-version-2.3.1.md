---
# SPDX-FileCopyrightText: 2022 CERN and the Allpix Squared authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Patch Release 2.3.1"
date: 2022-07-18T10:22:12+01:00
draft: false
---

We would like to announce the first patch release for the 2.3 series of **Allpix Squared, version 2.3.1**.
This release contains 28 commits over the feature release version 2.3.0.
The release is available as Docker image, CVMFS installation, binary tarball and source code from the [repository](https://gitlab.cern.ch/allpix-squared/allpix-squared/).

The following issues have been resolved and improvements have been made:
<!--more-->


* **Core**
   * Make the executable code compatible with Apple M1 processors [MR!807](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/807).
   * Switching to `boost:uniform_distributions`. Since the implementation is the same as for the C++ STL we do not expect any differences in the simulation from this change, but have done it for consistency and to be future-proof against changes of the (non-guaranteed) STL implementation (for more info, [here the original MR!375 when switching to Boost.Random](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/375)).
* **Geant4 Interface:**:
   * User limits on track length and total tracking time are now correctly set on the world *region* instead of on the world logical volume. With this change, user limits are correctly propagated to daughter logical volumes in the same region.
   * Now `G4ExceptionHandler`s are registered for each of the run managers, also in multi-threaded mode. This way, all G4 exceptions are properly caught and processed by our framework. Exceptions of type `JustWarning` are logged on `WARNING` level and caught.
   * It is now possible to configure the Geant4 tracking verbosity via the `geant4_tracking_verbosity` parameter of the *DepositionGeant4* module. The default value is `0`, higher levels mean more output. It should be noted that the respective log output is redirected to the logging level set via the `log_level_g4cout` parameter configured in the *GeometryBuilderGeant4* module.
* **Modules ProjectionPropagation, GenericPropagation, TransientPropagation:**
    * Added new parameter `max_charge_groups` which allows to limit the number of propagation groups generated from a single deposit. By default, this feature is turned off, but when activated this can be used to limit computation time on high-energy deposition events. See [MR!800](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/800).
    * Fixed an issue in the calculation of the final *local time* of PixelCharge objects generated by the modules. There was a double counting of the initial arrival time of the particle in the sensor.
* **Module DepositionReader:** Fixed a problem with NULL-termination of char strings read from ROOT trees, see [MR!774](https://gitlab.cern.ch/allpix-squared/allpix-squared/-/merge_requests/774).
