---
# SPDX-FileCopyrightText: 2022 CERN and the Allpix Squared authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Allpix Squared User Manual"
linkTitle: "Documentation"
menu:
  main:
    weight: 40
    pre: <i class='fa-solid fa-book'></i>
---

**This is the online version of the documentation for Allpix Squared. To download the PDF version visit
[this page](https://project-allpix-squared.web.cern.ch/usermanual/).**
