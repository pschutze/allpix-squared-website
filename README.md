---
# SPDX-FileCopyrightText: 2017-2022 CERN and the Allpix Squared authors
# SPDX-License-Identifier: CC-BY-4.0
---

# Allpix Squared Website

This repository contains the static website pages of the Allpix Squared project. The deployed website can be found
[here](https://allpix-squared.docs.cern.ch/).

All commits to the master branch of this repository are automatically deployed via GitLab pages and thus
directly reflect on the published website.

## Dependencies

The website is generated from markdown files using the [hugo](https://gohugo.io/) compiler written in Go. In order to
install Hugo on your machine, pick the appropriate package or binary from
[the latest release](https://github.com/gohugoio/hugo/releases). Since we use
[hugo modules](https://gohugo.io/hugo-modules/), one also needs the `go` binary, which can be downloaded
[here](https://go.dev/doc/install).

## Commands
The following commands might help adding content and testing the website locally before committing and deployment:

* __Start a local test server__
    ```sh
    ./get_artifacts.sh ref  # fetches the docs from the main repo
    hugo server -D
    ```
    The server will listen to [http://localhost:1313/](http://localhost:1313/).
    The additional command line argument `-D` also enables all documents currently in draft mode.

    If you want to test changes e.g. for a merge request, you can either supply a reference to your branch like this:
    ```sh
    ./get_artifacts.sh ref branch_name user_name/repo_name  # default: master allpix-squared/allpix-squared
    ```
    However this might not work if your pipeline is failing. To select artifacts from a specific job, use this:
    ```sh
    ./get_artifacts.sh job job_nr  # you can find job_nr in the URL of the specific docs:usermanual-hugo job
    ```

    Note: in both cases the repository has to be hosted on the CERN GitLab instance for the CI to work.

* __Add a new page__
    In order to add a new static page, run
    ```sh
    hugo new content/pagetitle.md
    ```
    and a new markdown file with the appropriate header will be created. The draft status is set to `true` by default.

* __Add a new post to the blog roll__
    In order to add a new post to the blog/new roll, run
    ```sh
    hugo new content/post/YYYY-MM-DD-post-title.md
    ```
    and a new markdown file with the appropriate header will be created. The draft status is set to `true` by default.
    For releases, change `news` to `releases`. Make sure to add a properly formatted date in your filename.

* __Add page to the main menu__

    See for example [content/docs/_index.md](./content/docs/_index.md) how to define a page to be in the main menu.

* __Test API reference__
    To test the website with the API reference, download it via
    ```sh
    ./get_artifacts.sh ref doxy  # fetches the API reference from the main repo
    ```
    or
    ```sh
    ./get_artifacts.sh job doxy job_nr  # fetches the API reference for a specific CI job
    ```

## Deploy Changes

Simply push to the `master` branch of this repository to get things published. It should be noted that only documents with
`draft = false` will be included in the published website.

## Updating Docsy & Prism

* __Docsy__
    To update Docsy (our hugo theme), run
    ```sh
    hugo mod get -u
    ```
    The currently used version is v0.6.0 from December 2022.

* __Prism__
    To update Prism (our code highlighter), visit [this page][@prismdownload] and download `prism.js` and `prism.css` to the
    `static/js` and `static/css` folder respectively. Note that if you change any settings, make sure to update this README
    with the corresponding URL as well.
    The currently used version is 1.29.0 from August 2022.

* __Roboto__
    Roboto is the main font for the website. It is usually included via Google Fonts, however we want to avoid pinging
    Google Servers for legal reason, thus the font is self-hosted and needs to be updated from time to time. To update the
    fonts, visit [this page][@robotodownload] and select `latin`, `latin-ext`, `greek`, `greek-ext` with font size `300`,
    `regular`, `500`, `700`, `300italic`, `italic`, `500italic`, `700italic` in the `Modern Browsers` format. Then copy the
    `.woff2` files to `static/webfonts`.
    The currently used version is v30.

* __Font Awesome__
    Font Awesome is self-hosted, so it needs to be updated from time to time. Use the "Free For Web" download from
    [here][@fadownload] and copy the `.woff2` files to `static/webfonts`.
    The currently used version is v6.4.0 from March 2023.

[@prismdownload]: https://prismjs.com/download.html#themes=prism-tomorrow&languages=clike+bash+c+cpp+cmake+ini+yaml&plugins=autolinker+show-language+toolbar+copy-to-clipboard
[@robotodownload]: https://gwfh.mranftl.com/fonts/roboto?subsets=greek,greek-ext,latin,latin-ext
[@fadownload]: https://fontawesome.com/download

## CERN webEOS setup

This is a more technical section mainly interesting for developers documenting the setup of the website.

The main website is deployed via GitLab pages, as documented in [how-to.docs.cern.ch](https://how-to.docs.cern.ch/). The
project name on webEOS ([webeos.cern.ch](https://webeos.cern.ch)) is `allpix-squared` and it is managed by the
`cernbox-project-allpix-squared-admins` eGroup. It only is used by the "Gitlab Pages Site Operator" to publish the hugo
generated page to [allpix-squared.docs.cern.ch](https://allpix-squared.docs.cern.ch/).

The old website was hosted on [project-allpix-squared.cern.ch](https://project-allpix-squared.web.cern.ch), with the project
name `project-allpix-squared` on webEOS. It is still used in the `deploy:eos` job in the main repo to deploy the PDF
documentation to [project-allpix-squared.web.cern.ch/usermanual](https://project-allpix-squared.web.cern.ch/usermanual/).

If one is member of the `cernbox-project-allpix-squared-admins` eGroup that manages `project-allpix-squared.web.cern.ch`,
the webroot can be accessed via [CERNBox](https://cernbox.cern.ch) (Your Projects -> allpix-squared -> www).

To ensure links to the old website still work fine, redirects have been created using these HTML headers:
```html
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="refresh" content="0; url='https://allpix-squared.docs.cern.ch/post/some-news-entry/'" />
</head>
</html>
```
